<title>ZDASH</title>
<link rel="stylesheet" href="/app/zdash/css/global/colors.css">
<link rel="stylesheet" href="/app/zdash/css/global/font.css">
<link rel="stylesheet" href="/app/zdash/css/global/typo.css">
<link rel="stylesheet" href="/app/zdash/css/global/page.css">
<link rel="stylesheet" href="/app/zdash/frameworks/vicon/style.css">
<link rel="stylesheet" href="/app/zdash/css/dashboard/layout.css">
<link rel="stylesheet" href="/app/zdash/css/dashboard/test.css">
<script data-modules="resizer, contentLoader" src="http://library.vilshub.com/lib/vUX/4.0.0/vUX-4.0.0.beta.js"></script>
<script data-src="/app/zdash/js/dashboard/nav.js"></script>
<script data-src="/app/zdash/js/dashboard/user-action.js"></script>
<script data-src="/app/zdash/js/dashboard/content-loader.js"></script>