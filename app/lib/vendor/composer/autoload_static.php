<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit01291a4741376af5afe27525bf3e6abb
{
    public static $files = array (
        '0aea32267f3759193556634fd722dbff' => __DIR__ . '/../../../..' . '/app/lib/functions/system/navigator.php',
        '9bf276fe800f891c150bcf2557cfd5c3' => __DIR__ . '/../../../..' . '/app/lib/functions/system/output.php',
    );

    public static $prefixLengthsPsr4 = array (
        'v' => 
        array (
            'vilshub\\validator\\' => 18,
            'vilshub\\router\\' => 15,
            'vilshub\\http\\' => 13,
            'vilshub\\helpers\\' => 16,
            'vilshub\\dbant\\' => 14,
        ),
        'D' => 
        array (
            'Dice\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'vilshub\\validator\\' => 
        array (
            0 => __DIR__ . '/..' . '/vilshub/validator/src',
        ),
        'vilshub\\router\\' => 
        array (
            0 => __DIR__ . '/..' . '/vilshub/router/src',
        ),
        'vilshub\\http\\' => 
        array (
            0 => __DIR__ . '/..' . '/vilshub/http/src',
        ),
        'vilshub\\helpers\\' => 
        array (
            0 => __DIR__ . '/..' . '/vilshub/helpers/src',
        ),
        'vilshub\\dbant\\' => 
        array (
            0 => __DIR__ . '/..' . '/vilshub/dbant/src',
        ),
        'Dice\\' => 
        array (
            0 => __DIR__ . '/..' . '/level-2/dice',
        ),
    );

    public static $classMap = array (
        'App' => __DIR__ . '/../../../..' . '/app/lib/classes/system/core/App.php',
        'Auth' => __DIR__ . '/../../../..' . '/app/lib/classes/application/middlewares/Auth.php',
        'Boot' => __DIR__ . '/../../../..' . '/app/lib/classes/system/core/Boot.php',
        'CSRF' => __DIR__ . '/../../../..' . '/app/lib/classes/system/helpers/CSRF.php',
        'CheckIP' => __DIR__ . '/../../../..' . '/app/lib/classes/application/middlewares/checkIP.php',
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'Console' => __DIR__ . '/../../../..' . '/app/lib/classes/system/core/Console.php',
        'Controller' => __DIR__ . '/../../../..' . '/app/lib/classes/system/core/Controller.php',
        'Cookie' => __DIR__ . '/../../../..' . '/app/lib/classes/system/helpers/Cookie.php',
        'DataParser' => __DIR__ . '/../../../..' . '/app/lib/classes/system/helpers/DataParser.php',
        'ErrorHandler' => __DIR__ . '/../../../..' . '/app/lib/classes/system/helpers/ErrorHandler.php',
        'FileInfo' => __DIR__ . '/../../../..' . '/app/lib/classes/system/helpers/FileInfo.php',
        'Hasher' => __DIR__ . '/../../../..' . '/app/lib/classes/system/helpers/Hasher.php',
        'Loader' => __DIR__ . '/../../../..' . '/app/lib/classes/system/helpers/Loader.php',
        'Middleware' => __DIR__ . '/../../../..' . '/app/lib/classes/system/core/Middleware.php',
        'Model' => __DIR__ . '/../../../..' . '/app/lib/classes/system/core/Model.php',
        'Route' => __DIR__ . '/../../../..' . '/app/lib/classes/system/helpers/Route.php',
        'SampleModel' => __DIR__ . '/../../../..' . '/app/lib/classes/application/models/SampleModel.php',
        'Session' => __DIR__ . '/../../../..' . '/app/lib/classes/system/helpers/Session.php',
        'Users' => __DIR__ . '/../../../..' . '/app/lib/classes/application/controllers/Users.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit01291a4741376af5afe27525bf3e6abb::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit01291a4741376af5afe27525bf3e6abb::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit01291a4741376af5afe27525bf3e6abb::$classMap;

        }, null, ClassLoader::class);
    }
}
