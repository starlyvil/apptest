addEventListener("load", function(){
    assign();
}, false)

function assign(){
    var submenu = document.querySelectorAll(".vNav.h-menu li ul li");
	var menu = document.querySelectorAll(".vNav.h-menu > li");
    var links = document.querySelectorAll(".vNav.h-menu li a");

    var activeMenu = null;    
    var zIndex = 1;
    for(x=0;x<menu.length;x++){
        menu[x].onmouseenter = function(){
            
            if(this.classList.contains("has-sub")){
                activeMenu = this;
            }
    
            var dropDown =  this.querySelector("li>ul");

            if(dropDown != null){
                dropDown.style["display"] = "block";               
                var height = dropDown.scrollHeight;            
                dropDown.classList.add("show");
                dropDown.style["height"] = height+"px";
                dropDown.style["font-size"] = "13px";
            }
        }

        menu[x].onmouseleave = function(){
            gg =  this.querySelector("li ul");
            if(gg != null){		
                var t = activeMenu.querySelector("ul");
                t.classList.remove("show")
                t.style["overflow"] = "hidden";
            
                gg.style["display"] = "none";
                gg.style["height"] = "0px";
                gg.style["font-size"] = "0px";
            }
        }
    }

    for(x=0;x<submenu.length;x++){
        submenu[x].onmouseenter = function(){
            var slideOutSubMenu =  this.querySelector("li>ul");

            if(slideOutSubMenu != null){
                slideOutSubMenu.style["font-size"] = "0";
                slideOutSubMenu.style["display"] = "block";
                rect = this.getBoundingClientRect()
                width = 130;
                zIndex++;
                if ((rect.right+width) > innerWidth) {
                    pos = "right";
                    pos2 = "left";
                }else{
                    pos = "left";
                    pos2 = "right";
                }

                slideOutSubMenu.scrollWidth;
                slideOutSubMenu.style[pos2] = "auto";
                slideOutSubMenu.style[pos] = "100%";
                
                slideOutSubMenu.style["width"] = width+"px";
                slideOutSubMenu.style["font-size"] = "13px";
                slideOutSubMenu.style["z-index"] = zIndex;
            }
        }

        submenu[x].onmouseleave = function(){
            var slideOutSubMenu =  this.querySelector("li ul");
            if(slideOutSubMenu != null){
                slideOutSubMenu.style["display"] = "none";
                slideOutSubMenu.style["width"] = "0";
            }
        }
    }
    
    for(x=0;x<links.length;x++){
        links[x].addEventListener("mouseenter", function(e){
            var ele = e.target;
            if(ele.parentNode.classList.contains("has-sub")){// 
                ele.classList.add("activeItem");
            }else if(ele.parentNode.classList.contains("sub")){
                ele.classList.add("activeItem");
            }
        }, false)
    }

    document.addEventListener("transitionend", function(e){
        if(e.target.parentNode.classList.contains("has-sub") && e.target.classList.contains("show")){// is drop down, enable visibility
            e.target.style["overflow"] = "visible";
        }
    },false);		
}