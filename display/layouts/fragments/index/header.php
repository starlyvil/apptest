<header id="headerBoundary">
    <div id="headerContent">
        <div id="logoCon">
            <img src="/assets/imgs/logo.jpg" alt="Clink Logo">
        </div>
        <div id="m-logocon"></div>
        <div  id="h-nav">
            <ul class="vNav h-menu">
                <li ><a href="/" class="activeMenu main-menu">Home</a></li>
                <li class="has-sub">
                        <a class="main-menu">Companies</a>
                        <ul>
                            <li><a href="">Finance</a></li>
                            <li class="sub"><a>Energy</a>
                                <ul>
                                    <li><a href="">Renewable</a></li>
                                    <li class="sub"><a>Petroleum</a>
                                        <ul>
                                            <li><a>Crude oil</a></li>
                                            <li><a>Natural gas</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="">Entertainment</a></li>
                        </ul>
                </li>
                <li class="has-sub">
                        <a class="main-menu">Freelancers</a>
                        <ul>
                            <li><a href="">Designers</a></li>
                            <li class="sub"><a>Developers</a>
                                <ul>
                                    <li><a href="">Frontend</a></li>
                                    <li><a href="">Backend</a></li>
                                    <li><a href="">Fullstack</a></li>
                                </ul>
                            </li>
                            <li><a href="">Writers</a></li>
                        </ul>
                </li>
                <li><a href="" class="main-menu">Sign In</a></li>
            </ul>
        </div>
        <div id="mobile-menu">
            <div id="mobile-button-con">
                <button type="button"></button>
            </div>

        </div>
    </div>
</header>

<section id="contentBoundary"> 