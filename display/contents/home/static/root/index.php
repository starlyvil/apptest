<div id="homeBox">
    <div id="searchBox">
        <div id="searchContent">
            <p>Search for <span>....</span></p>
            <div id="searchInputField">
                <div class="iwrapper s-input">
                    <input type="text" name="" id="searchInput">
                </div>
                <div class="iwrapper c-box">
                    <input type="checkbox" name="" id="targetObject" class="slideSwitch" data-labels= "Freelancer, Company" data-dim="100px, 30px">
                </div>
            </div>
            <div class="iwrapper btn">
                <button type="button" id="search-bt">Search</button>
            </div>
        </div>
    </div>
</div>